package stepsdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.thucydides.core.util.SystemEnvironmentVariables;
import tasks.LoginTrelloTask;
import ui.TrelloPage;
import utils.ReservedTexts;
import utils.TestTexts;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class TrelloStepDefinitions extends BaseTest {

    public TrelloStepDefinitions(){}

    @Given("^Open browser in the URL$")
    public void openBrowserInTheURL(){
        theActorInTheSpotlight().wasAbleTo(Open.url(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(ReservedTexts.URL.getTexts())));
    }

    @When("^Introduce credentials$")
    public void introduceCredentials(){
        theActorInTheSpotlight().attemptsTo(LoginTrelloTask.execute());
    }

    @Then("^Verify home page from TRELLO")
    public void verifyHomePageFromTRELLO(){
        theActorInTheSpotlight().attemptsTo(Ensure.that(TrelloPage.LBL_USER).attribute("title").isEqualTo(TestTexts.LBL_USER.getTexts()));
    }
}

