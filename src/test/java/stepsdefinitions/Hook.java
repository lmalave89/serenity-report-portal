package stepsdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import utils.ReportPortal.TakeScreenshot;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class Hook extends BaseTest{

    public Hook(){}

    @After
    public void close(Scenario scenario){
        if (scenario.isFailed()) {
            TakeScreenshot.now(theActorInTheSpotlight(), "Failed scenario test");
        }else{
            TakeScreenshot.now(theActorInTheSpotlight(), "Finished scenario test");
        }
    }
}
