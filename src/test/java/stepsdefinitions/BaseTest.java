package stepsdefinitions;

import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class BaseTest {

    @Managed(driver = "chrome")
    private  WebDriver hisBrowser;
    public static String actor = "user";

    public BaseTest(){
        OnStage.setTheStage(new OnlineCast());
        theActorCalled(actor).can(BrowseTheWeb.with(hisBrowser));
    }
}

