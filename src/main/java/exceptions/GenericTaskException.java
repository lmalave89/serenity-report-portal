package exceptions;

import net.serenitybdd.core.exceptions.SerenityManagedException;

public class GenericTaskException extends SerenityManagedException {
    public static final String error = "\n Failed executing the task";
    public GenericTaskException(String message, Throwable testErrorException) {
        super(message + error, testErrorException);
    }
}
