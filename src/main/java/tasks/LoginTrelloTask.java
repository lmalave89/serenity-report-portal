package tasks;

import exceptions.GenericTaskException;
import interactions.ClickIteraction;
import interactions.WritteIteraction;
import models.UserTrello;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.thucydides.core.annotations.Shared;
import ui.TrelloPage;
import utils.ReportPortal.TakeScreenshot;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LoginTrelloTask implements Task {

    @Shared
    public UserTrello userTrello;

    @Override
    public <T extends Actor> void performAs(T actor) {
        try{
            actor.attemptsTo(ClickIteraction.in(TrelloPage.BTN_SIGN_IN_HOME_PAGE));
            actor.attemptsTo(WritteIteraction.in(TrelloPage.INPUT_USERNAME, userTrello.getUsername()));
            actor.attemptsTo(ClickIteraction.in(TrelloPage.BTN_SIGN_IN_LOGIN_ATLASSIAN));
            actor.attemptsTo(WritteIteraction.in(TrelloPage.INPUT_PASSWORD, userTrello.getPassword()));
            actor.attemptsTo(ClickIteraction.in(TrelloPage.BTN_SIGN_IN_LOGIN));
            TakeScreenshot.now(actor, LoginTrelloTask.class.getName());
        }catch(Exception e){
            new GenericTaskException(GenericTaskException.error, e);
        }
    }

    public static LoginTrelloTask execute(){
        return instrumented(LoginTrelloTask.class);
    }
}
