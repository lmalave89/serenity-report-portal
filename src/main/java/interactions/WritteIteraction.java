package interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import utils.ReportPortal.TakeScreenshot;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class WritteIteraction implements Interaction {

    private Target element;
    private String text;

    public WritteIteraction(Target element, String text){
        this.element = element;
        this.text = text;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(WaitUntil.the(this.element, isVisible()));
        actor.attemptsTo(Enter.theValue(this.text).into(this.element));
        TakeScreenshot.now(actor, WritteIteraction.class.getName());
    }

    public static WritteIteraction in(Target element, String text){
        return new WritteIteraction(element, text);
    }
}
