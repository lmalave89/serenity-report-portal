package interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import utils.ReportPortal.TakeScreenshot;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class ClickIteraction implements Interaction {

    private Target element;

    public ClickIteraction(Target element){
        this.element = element;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(WaitUntil.the(this.element, isVisible()));
        actor.attemptsTo(Click.on(this.element));
        TakeScreenshot.now(actor, ClickIteraction.class.getName());
    }

    public static ClickIteraction in(Target element){
        return new ClickIteraction(element);
    }

}
