package ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class TrelloPage extends PageObject {

    public static final Target BTN_SIGN_IN_HOME_PAGE = Target.the("Button Sign in home Page").located(By.xpath("/html/body/header/nav/div/a[1]"));
    public static final Target INPUT_USERNAME = Target.the("Input username").located(By.id("user"));
    public static final Target BTN_SIGN_IN_LOGIN_ATLASSIAN = Target.the("Button sign in login").located(By.id("login"));
    public static final Target INPUT_PASSWORD = Target.the("Input password").located(By.id("password"));
    public static final Target BTN_SIGN_IN_LOGIN = Target.the("Button sign in login").located(By.id("login-submit"));
    public static final Target LBL_USER = Target.the("Label project text").located(By.className("_2LKdO6O3n25FhC"));



}
