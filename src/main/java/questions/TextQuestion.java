package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.screenplay.waits.WaitUntil;
import utils.ReportPortal.TakeScreenshot;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class TextQuestion implements Question<String> {

    private Target element;

    public TextQuestion(Target element){
        this.element = element;
    }

    @Override
    public String answeredBy(Actor actor) {
        WaitUntil.the(this.element, isVisible());
        TakeScreenshot.now(actor, TextQuestion.class.getName());
        return this.element.resolveFor(actor).getText();
    }

    public static Question<String> value(Target element) {
        return new TextQuestion(element);
    }
}
