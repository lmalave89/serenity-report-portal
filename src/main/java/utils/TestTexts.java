package utils;

public enum TestTexts {

    LBL_USER("Luis Malave (luismalave5)");

    private final String text;

    TestTexts(String key) {
        this.text = key;
    }

    public String getTexts() {
        return this.text;
    }
}
