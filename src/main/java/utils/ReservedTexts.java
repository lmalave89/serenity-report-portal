package utils;

public enum ReservedTexts {

    URL("url"),
    QA("QA"),
    STG("STG"),
    UAT("UAT"),
    PROD("PROD"),
    ENV("env");

    private final String text;

    ReservedTexts(String key) {
        this.text = key;
    }

    public String getTexts() {
        return this.text;
    }

}
